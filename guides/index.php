<!DOCTYPE html>
<html>
<head>
<?php
function lang(){
	// default language is german
	$currentlang = "de";
	$lang = "en";
	$title = "Mobilizon kurz erkl&auml;rt";
	if(isset($_GET['lang']) && $_GET['lang']=="en") {
		$currentlang = "en";
		$lang = "de";
		$title  = "Mobilizon briefly explained";
	}
	return [ "select" => $lang, "current" => $currentlang, "title" => $title ];
    }

function theme(){
	// default theme is dark
	$theme = "light";
	$currenttheme = "dark";
	if(isset($_GET['theme']) && $_GET['theme']=="light") {
		  $theme = "dark";
		  $currenttheme = "light";
	  
	}
	return [ "select" => $theme, "current" => $currenttheme ];
    }
 
    $langar = lang();
    $lang = $langar["select"];
    $title = $langar["title"];
    $currentlang = $langar["current"];
    $themear = theme();
    $theme = $themear["select"];
    $currenttheme = $themear["current"];
?>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="github_<?php echo $currenttheme;?>.css">
<title><?php echo $title;?></title>
</head>

<body>
<article>
<button class="alignleft"><a href="index.php?lang=<?php echo $currentlang;?>&theme=<?php echo $theme;?>"><?php echo $theme;?> theme</a></button>
<button class="alignright"><a href="index.php?lang=<?php echo $lang;?>&theme=<?php echo $currenttheme?>"><?php echo $lang;?></a></button>
<div style="clear: both;"></div>
<?php
# insert body
$body = file_get_contents("body_".$currentlang.".html");
echo $body;
?>
</body>
</html>

